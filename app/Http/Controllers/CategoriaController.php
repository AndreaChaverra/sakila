<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Categoria;

class CategoriaController extends Controller
{
    //Accion: UN METODO DE CONTROLADOR
    //Nombre: puede ser cualquiera
    //recomendado el nombre en minuscula
    public function index(){
        $categorias = Categoria::paginate(5);
        //seleccionar las categorias existentes
        //enviar la coleccion de categorias existentes
        //y las vamos amostrar alli
        return view("categorias.index")->with("categorias", $categorias);

    }

    //mostrar el formulario de crear categoria
    public function create(){
        //echo "Formulario de Categorias";
        return view('categorias.new');
    }

    //legar los datos desde el formulario
    //guardar la categoria en BD

    public function store(Request $r){
        //validacion
        //1. establecer las reglas de validacion para cada campo
        $reglas = [
            "categoria" =>["required", "alpha"]
        ];

        $mensajes = [
        "required" => "campo obligatorio",
        "alpha" => "solo letras"
        ];


        //2. crear el objeto validador
        $validador = Validator::make($r->all(), $reglas, $mensajes);

        //3. validar: metodo fails
        //            retorna true(v) si la validacion falla
        //            retorna falso en caso de que los datos sean correctos
        if($validador->fails()){
            //codigo para cuando falla l validacion
            return redirect("categorias/create")->withErrors($validador);
        }else{
            //codigo cuando
        }



        //$_POST = arreglo en php
        //almacena la informacion que  viene
        //desde formularios
        //crear una nueva categoria
        $categoria = new Categoria();
        //trae campo desde el campo del formulario
        $categoria->name = $r->input("categoria");
        //guardar la nueva categoria
        $categoria->save();
        //redireccion con datos de sesion
        return redirect('categorias/create')->with("mensaje", "categoria Gaurdada");

    }

    public function edit($category_id){
        //seleccionar la categoria
        $categoria= Categoria::find($category_id);

        //mostrar la vista de actualizar categoria
        //llevando dentro la categoria
        return view("categorias.edit")->with("categoria", $categoria);


    }

    public function update($category_id){
        //seleccionar la categoriia de editar
        $categoria = Categoria::find($category_id);
        //editar sus atributos
        $categoria->name=$_POST["categoria"];
        //guardar cambios
        $categoria->save();
        //retornar formulario de edit
        return redirect("categorias/edit/$category_id")->with("mensaje", "categoria editada");
    }
}
