<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha256-0XAFLBbK7DgQ8t7mRWU5BF2OMm9tjtfH945Z7TTeNIo=" crossorigin="anonymous" />
    <title>Añadir Categoría</title>
</head>
<body>
<form method="POST" action='{{ url("categorias/update/$categoria->category_id") }}' class="form-horizontal">
<!--si la variable de sesion "mensaje" existe, la muestra-->
@if(session("mensaje"));
<p class="alert sucess"> {{session("mensaje") }} </p>
@endif
    @csrf
    <fieldset>
        <!-- Form Name -->
        <legend>Nueva Categoría</legend>
        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-4 control-label" for="textinput">Nombre Categoría:</label>
          <div class="col-md-4">
          <input id="textinput" name="categoria" value="{{$categoria->name}}" type="text" placeholder="" class="form-control input-md">
          <strong class="text-danger">{{$errors->first("categoria")}}</strong>
        </div>
        </div>
        <!-- Button -->
        <div class="form-group">
          <label class="col-md-4 control-label" for=""></label>
          <div class="col-md-4">
            <button id="" name="" class="btn btn-primary">Enviar</button>
          </div>
        </div>
        </fieldset>
        </form>
</body>
</html>
